<?php
$host = '31.28.241.184';
$port = '9000';
$null = NULL;
set_time_limit(120);
ob_implicit_flush();
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_bind($socket, 0, $port);
socket_listen($socket);
$main=array($socket);
$clients=array();
while (true) 
{
	if(count($clients)<2)
	{
		$changed=$main;
		socket_select($changed, $null, $null, 0, 10);
		if (in_array($socket, $changed))
		{
			$socket_new=socket_accept($socket);
			$clients[]=$socket_new;
			$header=socket_read($socket_new, 1024);
			perform_handshaking($header, $socket_new);
			$found_socket=array_search($socket, $changed);
			unset($changed[$found_socket]);
			$message=mask(count($clients));
			socket_write($socket_new,$message,strlen($message));
			if(count($clients)==2)
			{
				socket_write($clients[0],$message,strlen($message));
			}
		}
	}
	else
	{
		$changed=$clients;
		socket_select($changed, $null, $null, 0, 10);
		foreach ($changed as $changed_socket)
		{
			$buf=socket_read($changed_socket, 1024);
			$received_text=unmask($buf);
			if($received_text=='end')
			{
			break 2;
			}
			$key=array_search($changed_socket, $clients);
			$key_adress=($key+1)%2;
			$response_text=mask($key.'^'.$received_text);
			socket_write($clients[$key],$response_text,strlen($response_text));
			socket_write($clients[$key_adress],$response_text,strlen($response_text));
		}
	}
}
socket_close($sock);
function unmask($text) 
{
	$length=ord($text[1]) & 127;
	if($length == 126) 
	{
		$masks=substr($text, 4, 4);
		$data=substr($text, 8);
	}
	elseif($length == 127) 
	{
		$masks=substr($text, 10, 4);
		$data=substr($text, 14);
	}
	else 
	{
		$masks=substr($text, 2, 4);
		$data=substr($text, 6);
	}
	$text="";
	for ($i = 0; $i < strlen($data); ++$i) {
		$text .= $data[$i] ^ $masks[$i%4];
	}
	return $text;
}
function mask($text)
{
	$b1 = 0x80 | (0x1 & 0x0f);
	$length = strlen($text);
	
	if($length <= 125)
		$header = pack('CC', $b1, $length);
	elseif($length > 125 && $length < 65536)
		$header = pack('CCn', $b1, 126, $length);
	elseif($length >= 65536)
		$header = pack('CCNN', $b1, 127, $length);
	return $header.$text;
}
function perform_handshaking($receved_header,$client_conn)
{
	$headers = array();
	$lines = preg_split("/\r\n/", $receved_header);
	foreach($lines as $line)
	{
		$line = chop($line);
		if(preg_match('/\A(\S+): (.*)\z/', $line, $matches))
		{
			$headers[$matches[1]] = $matches[2];
		}
	}
	$secKey = $headers['Sec-WebSocket-Key'];
	$secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
	$upgrade  = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
	"Upgrade: websocket\r\n" .
	"Connection: Upgrade\r\n" .
	"Sec-WebSocket-Accept:$secAccept\r\n\r\n";
	socket_write($client_conn,$upgrade,strlen($upgrade));
}
